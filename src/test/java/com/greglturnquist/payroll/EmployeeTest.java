package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    @DisplayName("unit test get job title employee")
    void getJobTitle() {

        //arange
        Employee test= new Employee("nome","ultimo","descrição","titulo");

        //act
        String result= test.getJobTitle();

        //assert
        assertEquals("titulo",result);
    }

    @Test
    @DisplayName("unit test get job title employee- not equals")
    void getJobTitle_notEquals() {

        //arange
        Employee test= new Employee("nome","ultimo","descrição","titulo");

        //act
        String result= test.getJobTitle();

        //assert
        assertNotEquals("novoTitulo",result);
    }
    @Test
    @DisplayName("unit test set job title employee")
    void setJobTitle() {

        //arange
        Employee test= new Employee("nome","ultimo","descrição","titulo");

        //act
        test.setJobTitle("novoTitulo");
        String result= test.getJobTitle();

        //assert
        assertEquals("novoTitulo",result);
    }
    @Test
    @DisplayName("unit test set job title employee-not Equals")
    void setJobTitle_notEquals() {

        //arange
        Employee test= new Employee("nome","ultimo","descrição","titulo");

        //act
        test.setJobTitle("novoTitulo");
        String result= test.getJobTitle();

        //assert
        assertNotEquals("titulo",result);
    }
}